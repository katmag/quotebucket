(function() {
    'use strict';
    let currentAPI = 'prog';
    //TODO make it look human
    /***
     * @function
     * Quote getters
     */
    function getTrumpQuotes() {
        $.get("https://api.whatdoestrumpthink.com/api/v1/quotes/random", function(data){
            $('#quote-text').text(data.message);
            $('#quote-author').text('Donny T.');
        });
    }

    function getGoTQuotes() {
        $.get("https://got-quotes.herokuapp.com/quotes", function(data){
            $('#quote-text').text(data.quote);
            $('#quote-author').text(data.character);
        });
    }

    function getProgQuotes() {
        $.get("http://quotes.stormconsultancy.co.uk/random.json", function(data){
            $('#quote-text').text(data.quote);
            $('#quote-author').text(data.author);
        });
    }

    /***
     * @function
     * Fires a quote getter according to currently chosen API
     */
    function newQuote() {
        if (currentAPI === 'trump') {
            getTrumpQuotes();
        } else if (currentAPI === 'prog') {
            getProgQuotes();
        } else if (currentAPI === 'got') {
            getGoTQuotes();
        } else {
            getProgQuotes();
        }
    }

    /***
     * Button events
     */

    $('#trump').click(() => {
        currentAPI = 'trump';
        getTrumpQuotes();
    });

    $('#prog').click(() => {
        currentAPI = 'prog';
        getProgQuotes();
    });

    $('#got').click(() => {
        currentAPI = 'got';
        getGoTQuotes();
    });

    $('#btn-next').click(() => {
        newQuote();
    });

    $(document).ready(function() {
        getProgQuotes();

        $('body').keyup(function(e){
            if(e.keyCode == 32){
                newQuote();
            }
        });
    });
})();
