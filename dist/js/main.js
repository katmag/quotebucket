'use strict';

(function () {
    'use strict';

    var currentAPI = 'got';

    function getTrumpQuotes() {
        $.get("https://api.whatdoestrumpthink.com/api/v1/quotes/random", function (data) {
            console.log(data.message);
            $('#quote-text').text(data.message);
            $('#quote-author').text('-Donny T.');
        });
    }

    function getProgQuotes() {}

    function getGoTQuotes() {}

    $(document).ready(function () {
        getTrumpQuotes();
    });
})();