'use strict';
var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel');


gulp.task('default', ['watch']);

gulp.task('build-js', () => {
    return gulp.src('src/js/es6/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('dist/js/'));
});

// configure the jshint task
gulp.task('jshint', function() {
    return gulp.src('src/js/es5/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-css', function() {
    return gulp.src('src/styles/sass/*.scss')
        .pipe(sourcemaps.init())  // Process the original sources
        .pipe(sass())
        .pipe(sourcemaps.write()) // Add the map to modified source.
        .pipe(gulp.dest('dist/styles/'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
    gulp.watch('src/js/es6/*.js', ['jshint', 'build-js']);
    gulp.watch('src/styles/sass/*.scss', ['build-css']);
});